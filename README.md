Alpine Pipeline
---------------

This repository contains Alpine Linux docker images specifically intended to be used in [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines).

